const fs = require('fs');

async function createAndDeleteRandomJsonFiles(folderName,numberOfFiles){ 
    try{
        console.log(await createDirectory());
        console.log(await createRandomJsonFiles());
        console.log(await deleteTheCreatedFiles());
    }
    catch(err){
        console.error("The error is "+err);
    }

    function createDirectory(){
        return new Promise((resolve,reject)=>{
            fs.mkdir(folderName, err=>{
                if(err) reject(err);
                else resolve("Directory created successfully!");
            })
        })
    }

    function createRandomJsonFiles(){
        return new Promise((resolve,reject)=>{
            let data = JSON.stringify(require('./random.json'));
            for(let i=0;i<numberOfFiles;i++){
                fs.writeFile("test/test1/random"+(i+1)+".json", data, (err) => {
                    if (err) reject(err);
                    else resolve("File written successfully");
                })
            }
        })
    }

    function deleteTheCreatedFiles(){
        return new Promise((resolve,reject)=>{
            for(let i=0; i<numberOfFiles;i++){
                fs.unlink("test/test1/random"+(i+1)+".json", (err)=>{
                    if(err) reject(err);
                    else resolve("file is deleted");
                })
            }
        })
    }
}

module.exports = createAndDeleteRandomJsonFiles;
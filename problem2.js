const fs = require('fs');

async function fsCallbackProblem2(folderPath){
    try{
        let data = await readTxtfile("lipsum.txt");
        console.log("lipsum.txt is read");
        console.log(await createAndwriteFile(data.toUpperCase(),"upperCase.txt"));
        console.log(await updateFileNames("upperCase.txt"));
        data = await readTxtfile("upperCase.txt");
        console.log("upperCase.txt is read");
        console.log(await createAndwriteFile(data.toLowerCase().replaceAll(".",".\n"),"splitLowerCaseSentences.txt"))
        console.log(await updateFileNames("\nsplitLowerCaseSentences.txt"))
        data = await readTxtfile("splitLowerCaseSentences.txt");
        console.log("splitLowerCaseSentences.txt is read");
        console.log(await createAndwriteFile(data.split("\n").sort().join("\n"),"sorted.txt"));
        console.log(await updateFileNames("\nsorted.txt"));
        data = await readTxtfile("fileNames.txt");
        console.log("fileNames.txt is read");
        console.log(await deleteAll(data));
    }catch(err){
        console.error(err);
    }
    
    function readTxtfile(file){
        return new Promise((resolve,reject)=>{
            fs.readFile(folderPath+file, 'utf-8',(err,data) =>{
                if(err) reject(err);
                else resolve(data);
            })
        })
    }

    function updateFileNames(fileName){
        return new Promise((resolve,reject)=>{
            fs.appendFile(folderPath+"fileNames.txt", fileName, err=>{
                if(err) reject(err);
                else resolve("fileNames.txt updated.");
            })
        })
    }

    function createAndwriteFile(data, fileName){
        return new Promise((resolve,reject)=>{
            fs.writeFile(folderPath+fileName, data, err =>{
                if(err) reject(err);
                else resolve(fileName+" is created and written");
            })
        })
    }

    function deleteAll(data){
        return new Promise((resolve,reject)=>{
            let deleteList = data.split("\n");
            for(let i in deleteList){
                fs.unlink(folderPath+deleteList[i],err=>{
                    if(err) reject(err);
                    else resolve(deleteList+" are deleted");
                })
            }
        })
    }
}

module.exports = fsCallbackProblem2;